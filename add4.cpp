﻿#include <iostream>

int main()
{
	setlocale(LC_ALL, "Rus");
	int a, start = 0, end = 0, k = 0;
	for (int j = 0; j <= 999999; j++)
	{
		a = j;
		for (int i = 0; i < 6; i++)
		{
			if (i < 3)
				start += a % 10;
			else end += a % 10;
			a /= 10;
		}
		if (start == end)
			k++;
		start = end = 0;
	}
	std::cout << "Количество счастливых билетиков: " << k << " ";
}